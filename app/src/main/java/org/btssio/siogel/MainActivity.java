package org.btssio.siogel;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends Activity implements android.view.View.OnClickListener{
 private ImageButton identification;
 private ImageButton livraison;
 private ImageButton save;
 private ImageButton import1;
Intent id;
Intent li;
Intent sa;
Intent im;
ClientDAO unClient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        identification = (ImageButton) findViewById(R.id.identification);
        identification.setOnClickListener(this);
        livraison = (ImageButton) findViewById(R.id.livraison);
        livraison.setOnClickListener(this);
        save = (ImageButton) findViewById(R.id.sauvegarde);
        save.setOnClickListener(this);
        import1 = (ImageButton) findViewById(R.id.import1);
        import1.setOnClickListener(this);

        unClient=new ClientDAO(this);


       // testBD();
    }

    @Override
    public void onClick(View view) {
switch (view.getId()){
    case R.id.identification:
        id= new Intent(this,Id_activity.class);
        this.startActivity(id);
        break;
    case R.id.livraison:
        li= new Intent(this,AfficheListeClients.class);
        this.startActivity(li);
        break;
    case R.id.sauvegarde:
        sa= new Intent(this,Sa_Activity.class);
        this.startActivity(sa);
        break;
    case R.id.import1:
        im= new Intent(this,Im_Activity.class);
        this.startActivity(im);
        break;

}


        }
    public void testBD(){
Client cli3=new Client();


        //unClient.addClient1(new Client());
        //Log.i("message","msg3");

        ArrayList<Client> lesClients = unClient.getClients();
        for (Client c : lesClients)
        {
       //     Log.i("test client",c.getPrenom());
        }
        Client cli=unClient.getClient(1);
        //Log.i("test lecture",cli.getPrenom());
        cli.setSignatureBase64("test");
        unClient.modifsign(cli.getIdentifiant(),cli.getSignatureBase64());
        Client cli2=unClient.getClient(1);

        Log.i("test modif",cli2.getSignatureBase64()+"tttt");
         lesClients = unClient.getClients();
        for (Client c : lesClients)
        {
            Log.i("test client",c.getSignatureBase64()+"-");
        }


    }
}