package org.btssio.siogel;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ModificationClient extends Activity  implements  android.view.View.OnClickListener {
    TableLayout tableLayout;
    TextView identif;
    TextView nom, prenom, tel, addr, cp, ville, dateC, heuresouhait;
    EditText dateL, heureL, etat;
    ClientDAO inf;
    int a, l;
    String b, c2, d, e, f, g, h, i, j, k,m;
    String datelivraison, heurelivraison, etats;
    private Button geoloc, save, annule,signer,voir_signature;
    int id;
    Intent sauvegarder, geolocaliser, annuler,signe,voir_la_signature;
    Client c;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inf = new ClientDAO(this);
        id = this.getIntent().getExtras().getInt("id");
        c = inf.getClient(id);
        Log.i("test", c.toString());
        a = c.getIdentifiant();
        b = c.getNom();
        c2 = c.getPrenom();
        d = c.getTelephone();
        e = c.getAdresse();
        f = c.getCodePostal();
        g = c.getVille();
        h = c.getDateCommande();
        i = c.getHeureSouhaitLiv();
        j = c.getDateLivraison();
        k = c.getHeureReelleLivraison();
        l = c.getEtat();
        m = c.getSignatureBase64();
        setContentView(R.layout.activity_modification_client);
        tableLayout = (TableLayout) findViewById(R.id.table);
        identif = (TextView) this.findViewById(R.id.identif);
        identif.setText(" " + a + " ");
        nom = (TextView) this.findViewById(R.id.nom);
        nom.setText(" " + b + " ");
        prenom = (TextView) this.findViewById(R.id.prenom);
        prenom.setText(" " + c2 + " ");
        tel = (TextView) this.findViewById(R.id.telephone);
        tel.setText(" " + d + " ");
        addr = (TextView) this.findViewById(R.id.adresse);
        addr.setText(" " + e + " ");
        cp = (TextView) this.findViewById(R.id.cp);
        cp.setText(" " + f + " ");
        ville = (TextView) this.findViewById(R.id.ville);
        ville.setText(" " + g + " ");
        dateC = (TextView) this.findViewById(R.id.datecommande);
        dateC.setText(" " + h + " ");
        heuresouhait = (TextView) this.findViewById(R.id.heuresouhaite);
        heuresouhait.setText(" " + i + " ");
        dateL = (EditText) this.findViewById(R.id.datel);
        dateL.setText(" " + j);
        heureL = (EditText) this.findViewById(R.id.heurel);
        heureL.setText(" " + k);
        etat = (EditText) this.findViewById(R.id.etat);
        etat.setText( l+"");
        save = (Button) findViewById(R.id.save);
        save.setOnClickListener(this);
        annule = (Button) findViewById(R.id.annule);
        annule.setOnClickListener(this);
        geoloc = (Button) findViewById(R.id.geoloc);
        geoloc.setOnClickListener(this);
        signer= (Button) findViewById(R.id.signer);
        signer.setOnClickListener(this);
        voir_signature=(Button) findViewById(R.id.voir_signature);
        voir_signature.setOnClickListener(this);

    }

    protected void save() {

        dateL = (EditText) this.findViewById(R.id.datel);
        c.setDateLivraison(dateL.getText().toString());
        heureL = (EditText) this.findViewById(R.id.heurel);
        c.setHeureReelleLivraison(heureL.getText().toString());
        etat = (EditText) this.findViewById(R.id.etat);
        if (Integer.parseInt(etat.getText().toString()) < 4 && Integer.parseInt(etat.getText().toString()) > 0
        ) {
            c.setEtat(Integer.parseInt(etat.getText().toString()));

            inf.modifclient(c);
        }
        else
        {
            Toast.makeText(getApplicationContext(),"Il faut que l'état soit 1,2 ou 3 ", Toast.LENGTH_LONG).show();
        }


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.geoloc:
                geolocaliser = new Intent(this, Geolocalisation.class);
                geolocaliser.putExtra("id",id);

Log.i("test3","test avant geoloc");

                this.startActivity(geolocaliser);
                break;
            case R.id.save:
                save();
                sauvegarder = new Intent(this, AfficheListeClients.class);
                this.startActivity(sauvegarder);
                break;
            case R.id.signer:
                save();
                signe =new Intent(this,CaptureSignature.class);
                signe.putExtra("id",id);

                this.startActivity(signe);
                Log.i("test3","test avant signature");
                break;
            case R.id.voir_signature:
                if (m.length() > 0){
                    voir_la_signature = new Intent(this,AfficheSignature.class);
                    voir_la_signature.putExtra("id",id);
                    this.startActivity(voir_la_signature);
                }
                else {
                    Toast.makeText(this, "Pas de signature", Toast.LENGTH_LONG).show();
                }

                break;
            case R.id.annule:
                finish();
                break;
        }
    }
}