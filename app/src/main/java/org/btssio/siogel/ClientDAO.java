package org.btssio.siogel;



import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class ClientDAO {


    private static String base = "BDClient";
    private static int version = 2;
    private BdSQLiteOpenHelper accesBD;

    public ClientDAO(Context ct){
        accesBD = new BdSQLiteOpenHelper(ct, base, null, version);

    }

    public void addClient1(Client unClient){
        SQLiteDatabase bd = accesBD.getWritableDatabase();


        String req = "insert into client(id,nom,prenom,adresse,codepostal,ville,telephone,datecommande,heuresouhaitliv,heurereellelivraison,livraison,signature64,etat)"
                + " values("+unClient.getIdentifiant()+",'"+unClient.getNom()+"','"+unClient.getPrenom()+"','"+unClient.getAdresse()+"','"+unClient.getCodePostal()+"','"+unClient.getVille()+"','"+unClient.getTelephone()+"','"+unClient.getDateCommande()+"','"+unClient.getHeureSouhaitLiv()+"',"+unClient.getHeureReelleLivraison()+","+unClient.getDateLivraison()+","+unClient.getSignatureBase64()+","+unClient.getEtat()+");";
        //Log.i("message",req);
        Log.d("log",req);
        bd.execSQL(req);
        bd.close();
    }

   /* public long addViticulteur2(Viticulteur unViticulteur){
        long ret;
        SQLiteDatabase bd = accesBD.getWritableDatabase();

        ContentValues value = new ContentValues();
        value.put("nomV", unViticulteur.getNomV());
        value.put("niveauV",unViticulteur.getNiveauV());
        ret = bd.insert("viticulteur", null, value);

        return ret;
    }
*/

    public  Client getClient(int idC){
        Client leClient = null;
        Cursor curseur;
        curseur = accesBD.getReadableDatabase().rawQuery("select * from client where id="+idC+";",null);
        //Log.i("test","tes1");
        if (curseur.getCount() > 0) {
            curseur.moveToFirst();
            leClient = new Client(idC,curseur.getString(1), curseur.getString(2),curseur.getString(3),
                    curseur.getString(4),curseur.getString(5),curseur.getString(6),curseur.getString(7),
                    curseur.getString(8),curseur.getString(9),curseur.getString(10),curseur.getString(11),curseur.getInt(12));
        }

        return leClient;
    }

    public ArrayList<Client> getClients(){
        Cursor curseur;
        String req = "select * from client ";
        curseur = accesBD.getReadableDatabase().rawQuery(req,null);
        return cursorToClientArrayList(curseur);
    }

    public void modifsign(int id,String signature ){
        SQLiteDatabase db =accesBD.getWritableDatabase();
        String req;
       // Log.i("message","msg5");
        req = "update client set signature64='"+signature +"' where id="+id;
       // Log.i("message","msg6");
       db.execSQL(req);
       // Log.i("message","msg7");


    }
    public void modifclient(Client c ){
        SQLiteDatabase db =accesBD.getWritableDatabase();
        String req;
       // Log.i("message","msg5");
        req = "update client set heurereellelivraison='"+c.getHeureReelleLivraison() +"', livraison='"+c.getDateLivraison()+"', etat='"+c.getEtat()+"', signature64='"+c.getSignatureBase64()+"' where id="+c.getIdentifiant();
       // Log.i("message","msg6");
        db.execSQL(req);
      //  Log.i("message","msg7");


    }


    private ArrayList<Client> cursorToClientArrayList(Cursor curseur){
        ArrayList<Client> listeClient = new ArrayList<Client>();
        int id;
        String nom;
        String prenom;
        String adresse;
        String codepostal;
        String ville;
        String telephone;
        String datecommande;
        String heuresouhaitliv;
        String heurereellelivraison;
        String livraison;
        String signature64;
        int etat;

        curseur.moveToFirst();
        while (!curseur.isAfterLast()){
            id = curseur.getInt(0);
            nom = curseur.getString(1);
            prenom = curseur.getString(2);
            adresse = curseur.getString(3);
            codepostal = curseur.getString(4);
            ville = curseur.getString(5);
            telephone = curseur.getString(6);
            datecommande = curseur.getString(7);
            heuresouhaitliv = curseur.getString(8);
            heurereellelivraison = curseur.getString(9);
            livraison = curseur.getString(10);
            signature64= curseur.getString(11);
            etat = curseur.getInt(12);
            listeClient.add(new Client(id,nom,prenom,adresse,codepostal,ville,telephone,datecommande,heuresouhaitliv,heurereellelivraison,livraison,signature64,etat));
            curseur.moveToNext();
        }

        return listeClient;
    }
    //adresse,codepostal,ville,telephone,datecommande,heuresouhaitliv,heurereellelivraison,livraison,signature64,etat

}

