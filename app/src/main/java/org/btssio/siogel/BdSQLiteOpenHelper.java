package org.btssio.siogel;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BdSQLiteOpenHelper extends SQLiteOpenHelper {
    private String requete="create table client ("
            +"id INTEGER,"
            +"nom TEXT NOT NULL,"
            +"prenom TEXT NOT NULL,"
            +"adresse TEXT NOT NULL,"
            +"codepostal TEXT NOT NULL,"
            +"ville TEXT NOT NULL,"
            +"telephone TEXT NOT NULL,"
            +"datecommande TEXT NOT NULL,"
            +"heuresouhaitliv TEXT,"
            +"heurereellelivraison TEXT,"
            +"livraison TEXT,"
            +"signature64 TEXT,"
            +"etat INTEGER);";
    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(requete);

        db.execSQL("insert into client (id,nom,prenom,adresse,codepostal,ville,telephone,datecommande,heuresouhaitliv,heurereellelivraison,livraison,signature64,etat) " +
                "values('1','Sacha','Ketchum','30 rue du commencement','72000','Bourgpalette','0242655952','2 jours','12h','?','livré','Mr Sacha','1');");
        db.execSQL("insert into client (id,nom,prenom,adresse,codepostal,ville,telephone,datecommande,heuresouhaitliv,heurereellelivraison,livraison,signature64,etat) " +
                "values('2','Ondine','Misty','31 avenue eau','72000','Azuria','0245687896','2 jours','13heures','?','pas livré','Mme Ondine','0');");
        db.execSQL("insert into client (id,nom,prenom,adresse,codepostal,ville,telephone,datecommande,heuresouhaitliv,heurereellelivraison,livraison,signature64,etat) " +
               "values('3','Pierre','Brock','32 rue de la pierre','72000','Argenta','02432032474','2 jours','14heures','?','livré','MR Pierre','1');");
        db.execSQL("insert into client (id,nom,prenom,adresse,codepostal,ville,telephone,datecommande,heuresouhaitliv,heurereellelivraison,livraison,signature64,etat) " +
                        "values('4','Erika','Ortide','33 rue de la plante','72000','Celadopole','0243124578','2 jours','15heures','?','livré','Mme Erika','9');");
        db.execSQL("insert into client (id,nom,prenom,adresse,codepostal,ville,telephone,datecommande,heuresouhaitliv,heurereellelivraison,livraison,signature64,etat) " +
                        "values('5','Tanguy','Elec','59 rue des petites cours','37300','Joué-Lès-Tours','0252456315','2 jours','16 heures','?','livré','Mr Tanguy','1');");


                //ContentValues value = new ContentValues();
        //value.put("nomV", "Badoz");
        //value.put("niveauV",50);
        //db.insert("viticulteur", null, value);

    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub

    }
    public BdSQLiteOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        // TODO Auto-generated constructor stub
    }
}

