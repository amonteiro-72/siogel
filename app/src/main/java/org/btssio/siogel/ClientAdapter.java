package org.btssio.siogel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class ClientAdapter extends BaseAdapter {
    List<Client> listeClients;
    LayoutInflater layoutInflater;
    @Override
    public int getCount() {

        return listeClients.size();
    }

    @Override
    public Object getItem(int i) {
        return listeClients.get(i);
    }

    @Override
    public long getItemId(int position) {

        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Conteneur contenu;
        if (convertView == null){
            contenu = new Conteneur();
            convertView = layoutInflater.inflate(R.layout.vue_client,null);
            contenu.txtId=(TextView) convertView.findViewById(R.id.identifiant);
            contenu.txtNom=(TextView) convertView.findViewById(R.id.nom);
            contenu.txtPrenom=(TextView) convertView.findViewById(R.id.prenom);
            contenu.txtAdresse=(TextView) convertView.findViewById(R.id.adresse);
            contenu.txtTelephone=(TextView) convertView.findViewById(R.id.telephone);
            contenu.txtVille=(TextView) convertView.findViewById(R.id.ville);
            contenu.txtCodePostal=(TextView) convertView.findViewById(R.id.cp);

            convertView.setTag(contenu);

        }
        else {
            contenu = (Conteneur) convertView.getTag();
        }
        contenu.txtId.setText(""+listeClients.get(position).getIdentifiant());
        contenu.txtNom.setText(""+listeClients.get(position).getNom());
        contenu.txtPrenom.setText((""+listeClients.get(position).getPrenom()));
        contenu.txtAdresse.setText(""+listeClients.get(position).getAdresse());
        contenu.txtTelephone.setText(""+listeClients.get(position).getTelephone());
        contenu.txtVille.setText((""+listeClients.get(position).getVille()));
        contenu.txtCodePostal.setText(""+listeClients.get(position).getCodePostal());
        return convertView;
    }
    public ClientAdapter(Context context, List<Client> listeClients)
    {
        this.layoutInflater = LayoutInflater.from(context);
        this.listeClients = listeClients;
    }
    public class Conteneur {
        TextView txtId;
        TextView txtNom;
        TextView txtPrenom;
        TextView txtTelephone;
        TextView txtAdresse;
        TextView txtCodePostal;
        TextView txtVille;
    }

}
