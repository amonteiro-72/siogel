package org.btssio.siogel;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;

public class CaptureSignature extends AppCompatActivity implements  android.view.View.OnClickListener {
    LinearLayout vue_sign;
    ClientDAO inf;
    Client c;
    int id;
    String ligne1, ligne2;
    Button annuler, effacer, enregistrer;
    Signature laSignature;
    ImageView image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("test3","test dans capturesignature");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture_signature);
        vue_sign = (LinearLayout) findViewById(R.id.vue);
        inf = new ClientDAO(this);
        id = this.getIntent().getExtras().getInt("id");
        c = inf.getClient(id);
        ligne1 = "Client : "+c.getIdentifiant()+" "+c.getPrenom()+" "+c.getNom();
        ligne2 = "Livré le : "+c.getDateLivraison()+" à "+c.getHeureReelleLivraison();
        annuler = (Button) findViewById(R.id.btnAnnuleSign);
        annuler.setOnClickListener(this);
        effacer = (Button) findViewById(R.id.btnEffaceSign);
        effacer.setOnClickListener(this);
        enregistrer = (Button) findViewById(R.id.btnEnregSign);
        enregistrer.setOnClickListener(this);
        enregistrer.setEnabled(false);
        laSignature = new Signature(this, null, ligne1, ligne2);
        vue_sign.addView(laSignature, LinearLayout.LayoutParams.MATCH_PARENT);
        Log.i("test3","test fin capturesignature");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnEffaceSign:
                laSignature.reset();
                enregistrer.setEnabled(false);
                break;
            case R.id.btnEnregSign:
                c.setSignatureBase64(laSignature.save());
                inf.modifclient(c);
                finish();
                break;
            case R.id.btnAnnuleSign:
                finish();
                break;
        }

    }


    public class Signature extends View {
        String ligne1,ligne2;
        // variables nécessaire au dessin
        private Paint paint = new Paint();
        private Path path = new Path();// collection de l'ensemble des points sauvegardés lors des mouvements du doigt

        private Canvas canvas;
        private Bitmap bitmap;
        // constructeur
        public Signature(Context context, AttributeSet attrs,String ligne1,String ligne2) {
            super(context, attrs);
            this.ligne1=ligne1;
            this.ligne2=ligne2;

            this.setBackgroundColor(Color.WHITE);
            paint.setColor(Color.BLACK);
            paint.setStrokeWidth(5f);
            paint.setTextSize(25);


        }
        //gestion du dessin
        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            paint.setStyle(Paint.Style.FILL);
            canvas.drawText(ligne1, 20, 30, paint);
            canvas.drawText(ligne2, 20, 60, paint);
            paint.setStyle(Paint.Style.STROKE);
            canvas.drawPath(path, paint);
        }
        // gestion des événements du doigt
        @Override
        public boolean onTouchEvent(MotionEvent event) {
            enregistrer.setEnabled(true);
            float eventX = event.getX();
            float eventY = event.getY();
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    path.moveTo(eventX, eventY);
                    return true;
                case MotionEvent.ACTION_MOVE:
                    path.lineTo(eventX, eventY);
                    break;
                case MotionEvent.ACTION_UP:
// nothing to do
                    break;
                default:
                    return false;
            }
            invalidate();
            return true;


        }
        public String save() {
            String vretour;
            if (bitmap == null) { // si l’objet bitmap n’est pas initialisé
// il faut l’initialiser avec les tailles définie dans la vue du contexte
                bitmap = Bitmap.createBitmap(this.getWidth(), this.getHeight(),

                        Bitmap.Config.RGB_565);

            }
            try {
//
                canvas = new Canvas(bitmap);
                this.draw(canvas); // dessine la vue
// la classe ByteArrayOutputStream implémente un flux de sortie dans lequel les
// données sont écrites dans un tableau d'octets. La mémoire tampon augmente
// automatiquement à mesure que des données y sont écrites :
                ByteArrayOutputStream ByteStream = new ByteArrayOutputStream();
// Ecrit une version compressée du bitmap dans le flux de sortie défini précédemment
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, ByteStream);
// Création d’un tableau d'octets. Sa taille est la taille actuelle du flux de sortie.
                byte[] b = ByteStream.toByteArray();
// ce tableau est ensuite codé en String (utilisation pour cela d’un utilitaire de
// codage/décodage de la représentation Base64 de données binaires) :
                vretour = Base64.encodeToString(b, Base64.DEFAULT);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "Erreur Sauvegarde",

                        Toast.LENGTH_LONG).show();

                vretour = null;
            }
            return vretour;

        }
        public void reset(){
            path.reset();
            invalidate();
        }
        public void dessine(String encodedString) {
            try {
                byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
                bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
// puis « écriture » de l’image bitmap obtenue dans le composant correspondant du Layout en cours
// (dans notre cas une ImageView)
                image.setImageBitmap(bitmap);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "error dessine",
                        Toast.LENGTH_LONG).show();

            }
        }
    }












}

