package org.btssio.siogel;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Base64;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;

public class AfficheSignature extends Activity implements android.view.View.OnClickListener {
    Button retour;
    Intent ret;
    ClientDAO inf;
    Client c;
    int id;
    Signature laSignature;
    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inf = new ClientDAO(this);
        id = this.getIntent().getExtras().getInt("id");
        c=inf.getClient(id);

        setContentView(R.layout.activity_affiche_signature);
        retour = (Button) findViewById(R.id.retour);
        retour.setOnClickListener(this);
        image = (ImageView) findViewById(R.id.image);
        laSignature = new Signature(this,null);
        laSignature.dessine(c.getSignatureBase64());


    }

    @Override
    public void onClick(View view) {
       finish();
        }

    public class Signature extends View {

        private Bitmap bitmap;
        // constructeur
        public Signature(Context context, AttributeSet attrs) {
            super(context, attrs);


        }




        public void dessine(String encodedString) {
            try {
                byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
                bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
// puis « écriture » de l’image bitmap obtenue dans le composant correspondant du Layout en cours
// (dans notre cas une ImageView)
                image.setImageBitmap(bitmap);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "error dessine",
                        Toast.LENGTH_LONG).show();

            }
        }
    }


}