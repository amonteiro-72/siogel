package org.btssio.siogel;

public class Client {
    // Données qui ne seront pas « modifiables » par le livreur
    private int identifiant ;
    private String nom,prenom,adresse,codePostal,ville,telephone;
    private String dateCommande;
    private String heureSouhaitLiv; // créneau horaire de livraison souhaitée par le client

    public Client() {
        identifiant=0;
        nom="Dalola";
        prenom="Cynthia";
        adresse="59 rue des petites cours ";
        codePostal="37300";
        ville="Joué-Lès-Tours";
        telephone="06000100";
        dateCommande="21 janvier 2121";
        heureSouhaitLiv="/";
        heureReelleLivraison = null;
        dateLivraison = null;
        signatureBase64 = null;
        etat = 0;
    }

    public Client(int identifiant, String nom, String prenom, String adresse, String codePostal, String ville, String telephone, String dateCommande, String heureSouhaitLiv, String heureReelleLivraison, String dateLivraison, int etat) {
        this.identifiant = identifiant;
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.codePostal = codePostal;
        this.ville = ville;
        this.telephone = telephone;
        this.dateCommande = dateCommande;
        this.heureSouhaitLiv = heureSouhaitLiv;
        this.heureReelleLivraison = heureReelleLivraison;
        this.dateLivraison = dateLivraison;

        this.etat = etat;
    }

    public Client(int identifiant, String nom, String prenom, String adresse, String codePostal, String ville, String telephone, String dateCommande, String heureSouhaitLiv, String heureReelleLivraison, String dateLivraison, String signatureBase64, int etat) {
        this.identifiant = identifiant;
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.codePostal = codePostal;
        this.ville = ville;
        this.telephone = telephone;
        this.dateCommande = dateCommande;
        this.heureSouhaitLiv = heureSouhaitLiv;
        this.heureReelleLivraison = heureReelleLivraison;
        this.dateLivraison = dateLivraison;
        this.signatureBase64 = signatureBase64;
        this.etat = etat;
    }

    // Données « modifiables », qui seront à saisir lors d’une visite
    private String heureReelleLivraison; // pour simplifier l’heure sera une chaîne
    private String dateLivraison; // pour simplifier la date sera une chaîne
    private String signatureBase64;

    public int getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(int identifiant) {
        this.identifiant = identifiant;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getDateCommande() {
        return dateCommande;
    }

    public void setDateCommande(String dateCommande) {
        this.dateCommande = dateCommande;
    }

    public String getHeureSouhaitLiv() {
        return heureSouhaitLiv;
    }

    public void setHeureSouhaitLiv(String heureSouhaitLiv) {
        this.heureSouhaitLiv = heureSouhaitLiv;
    }

    public String getHeureReelleLivraison() {
        return heureReelleLivraison;
    }

    public void setHeureReelleLivraison(String heureReelleLivraison) {
        this.heureReelleLivraison = heureReelleLivraison;
    }

    public String getDateLivraison() {
        return dateLivraison;
    }

    public void setDateLivraison(String dateLivraison) {
        this.dateLivraison = dateLivraison;
    }

    public String getSignatureBase64() {
        return signatureBase64;
    }

    public void setSignatureBase64(String signatureBase64) {
        this.signatureBase64 = signatureBase64;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    private int etat;
    /* Exemples de situation client ------
     * 1 Absent lors de toutes les présentations du livreur
     * 2 Présent, mais refus de prendre livraison
     * 3 Présent et tout ok = livré
     */

    @Override
    public String toString() {
        return "Client{" +
                "identifiant=" + identifiant +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", adresse='" + adresse + '\'' +
                ", codePostal='" + codePostal + '\'' +
                ", ville='" + ville + '\'' +
                ", telephone='" + telephone + '\'' +
                ", dateCommande='" + dateCommande + '\'' +
                ", heureSouhaitLiv='" + heureSouhaitLiv + '\'' +
                '}';
    }
}
